# HAWAI-Produktkonfigurator

CI Status: ![CI Status](https://codeship.com/projects/46285060-d642-0132-59ed-1618a2fa1842/status?branch=master)

## Server Voraussetzungen

- Webserver mit PHP >= 5.4
- Mcrypt PHP Erweiterung
- OpenSSL PHP Erweiterung
- Mbstring PHP Erweiterung
- Tokenizer PHP Erweiterung
- Unterstützte Datenbank (z.B. MySQL, PGSQL)

## Installation

Mit Kommando `php artisan migrate` aktuelle Datenbankmigrationen installieren.
Virtual Host anlegen oder über einen Symlink auf das Verzeichnis `public` zeigen.

### Entwickler
- composer installieren
- mit `composer install` alle Abhängigkeiten installieren, mit `composer update` Abhängigkeiten aktualisieren
 
## Konfiguration
Im Rootverzeichnis eine `.env` Datei anlegen. Als Beispiel dient die `.env.example`. Hierbei ist folgendes zu beachten:

- APP_ENV - Name der Umgebung. Wichtig wenn Fallunterscheidung zwischen Produktiv- und Entwicklungssystem
- APP_DEBUG - Im Produktivsystem niemals auf true setzen!
- APP_KEY - Muss gesetzt werden. Sollte 32 Zeichen lang sein
- DB_HOST - In der Regel localhost
- DB_DATABASE - Name der Datenbank
- DB_USERNAME - Loginname des Datenbanknutzers
- DB_PASSWORD - Passwort des Datenbanknutzers
- CACHE_DRIVER, SESSION_DRIVER & QUEUE_DRIVER - Muss nicht geändert werden
- MAIL_DRIVER - zum Entwickeln log, dies schreibt E-Mail in Logfile. Produktiv SMTP mit entsprechenden Daten

## REST API
TODO: Kleine allgemeine Einleitung unserer REST API

### Rahmen
TODO: Anfrage und Antwort formulieren

### Komponenten
TODO: Anfrage und Antwort formulieren
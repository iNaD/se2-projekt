<?php namespace App\Http\Controllers;

class DraftController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Draft Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders our application's "draft"
	|
	*/

	/**
	 * Show the draft to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('draft.index');
	}

}
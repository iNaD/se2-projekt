<?php namespace App\Http\Controllers;

class ConfiguratorController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Configurator Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders our application's "configurator"
	|
	*/

	/**
	 * Show the configurator to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('configurator.index');
	}

}

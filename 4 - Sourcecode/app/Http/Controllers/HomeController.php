<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders our application's landing page
	|
	*/

	/**
	 * Show the application's landingpage to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

}

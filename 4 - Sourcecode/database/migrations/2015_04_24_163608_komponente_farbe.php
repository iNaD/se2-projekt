<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KomponenteFarbe extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('komponente_farbe', function(Blueprint $table)
		{
			$table->integer('komponente_id');
			$table->string('farbe');
			$table->foreign('farbe')->references('farbe')->on('farbe');
			$table->foreign('komponente_id')->references('komponente_id')->on('komponente');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('komponente_farbe');
	}

}
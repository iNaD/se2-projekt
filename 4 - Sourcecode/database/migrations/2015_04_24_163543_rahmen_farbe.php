<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RahmenFarbe extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rahmen_farbe', function(Blueprint $table)
		{
			$table->string('rahmen_typ');
			$table->string('farbe');
			$table->foreign('farbe')->references('farbe')->on('farbe');
			$table->foreign('rahmen_typ')->references('rahmen_typ')->on('rahmen');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rahmen_farbe');
	}

}
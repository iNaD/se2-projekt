<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntwurfKomponente extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entwurf_komponente', function(Blueprint $table)
		{
			$table->integer('entwurf_id');
			$table->integer('komponente_id');
			$table->string('komponente_farbe');
			$table->foreign('entwurf_id')->references('entwurf_id')->on('entwurf');
			$table->foreign('komponente_id')->references('komponente_id')->on('komponente');
			$table->foreign('komponente_farbe')->references('farbe')->on('farbe');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entwurf_komponente');
	}

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entwurf extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entwurf', function(Blueprint $table)
		{
			$table->integer('entwurf_id')->unique();
			$table->integer('user_id');
			$table->string('rahmen_typ');
			$table->string('rahmen_farbe');
			
			$table->foreign('rahmen_typ')->references('rahmen_typ')->on('rahmen');
			$table->foreign('user_id')->references('id')->on('user');
			$table->foreign('rahmen_farbe')->references('farbe')->on('farbe');
			$table->primary('entwurf_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entwurf');
	}

}
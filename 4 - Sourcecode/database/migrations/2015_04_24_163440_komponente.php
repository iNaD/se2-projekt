<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Komponente extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('komponente', function(Blueprint $table)
		{
			$table->integer('komponente_id')->unique();
			$table->string('komponente_typ');
			$table->string('bezeichnung');
			$table->primary('komponente_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('komponente');
	}

}
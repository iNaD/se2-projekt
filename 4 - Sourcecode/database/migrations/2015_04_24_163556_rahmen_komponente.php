<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RahmenKomponente extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rahmen_komponente', function(Blueprint $table)
		{
			$table->string('rahmen_typ');
			$table->integer('komponente_id');
			$table->float('preis');
			$table->foreign('rahmen_typ')->references('rahmen_typ')->on('rahmen');
			$table->foreign('komponente_id')->references('komponente_id')->on('komponente');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rahmen_komponente');
	}

}
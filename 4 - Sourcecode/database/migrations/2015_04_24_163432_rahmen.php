<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rahmen extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rahmen', function(Blueprint $table)
		{
			$table->string('rahmen_typ')->unique();
			$table->float('preis');
			$table->primary('rahmen_typ');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rahmen');
	}

}
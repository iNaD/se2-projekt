<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->integer('id')->unique();
			$table->string('e-mail')->unique();
			$table->string('vorname');
			$table->string('nachname');
			$table->string('passwort');
			$table->string('adresse');
			$table->date('geb datum');
			$table->float('groesse');
			$table->float('schrittlaenge');
			$table->string('geschlecht');
			$table->primary('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
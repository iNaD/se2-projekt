<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();

            $this->call('UserSeeder');
            
            $this->command->info('User seeded');
            
            $this->call('RahmenSeeder');
            
            $this->command->info('Rahmen seeded');
            
            $this->call('KomponenteSeeder');
            
            $this->command->info('Komponente seeded');
            
            $this->call('FarbeSeeder');
            
            $this->command->info('Farbe seeded');
            
            $this->call('Rahmen_KomponenteSeeder');
            
            $this->command->info('Rahmen Komponente seeded');
            
            $this->call('EntwurfSeeder');
            
            $this->command->info('Entwurf seeded');
            
            $this->call('Entwurf_KomponenteSeeder');
            
            $this->command->info('Entwurf Komponente seeded');
		
	}
        
        

}

class UserSeeder extends Seeder {
	public function run()
	{
		DB::table('user')->insert(['id' => 1, 'e-mail' => 'clark.kent@krypton.com', 'vorname' => 'clark', 'nachname' => 'kent', 
                    'passwort' => 'superman', 'adresse' => 'kryptonstraße 1, krypton', 'geb datum' => date('1985-01-01'), 
                    'groesse' => 1.95, 'schrittlaenge' => 130, 'geschlecht' => 'männlich']);
                
                DB::table('user')->insert(['id' => 2, 'e-mail' => 'peter.parker@spidey.com', 'vorname' => 'peter', 'nachname' => 'parker', 
                    'passwort' => 'spiderman', 'adresse' => 'queens 1, new york', 'geb datum' => date('1989-01-01'), 
                    'groesse' => 1.95, 'schrittlaenge' => 120, 'geschlecht' => 'männlich']);
                
                DB::table('user')->insert(['id' => 3, 'e-mail' => 'bruce.wayne@gotham.com', 'vorname' => 'bruce', 'nachname' => 'wayne', 
                    'passwort' => 'badman', 'adresse' => 'gothamweg 5, gotham', 'geb datum' => date('1980-01-01'), 
                    'groesse' => 2.02, 'schrittlaenge' => 140, 'geschlecht' => 'männlich']);
                
                DB::table('user')->insert(['id' => 4, 'e-mail' => 'ash.ketchum@pika.com', 'vorname' => 'ash', 'nachname' => 'ketchum', 
                    'passwort' => 'pantimos', 'adresse' => 'alabastia 1, vertania city', 'geb datum' => date('1996-01-01'), 
                    'groesse' => 1.60, 'schrittlaenge' => 80, 'geschlecht' => 'männlich']);
                
                DB::table('user')->insert(['id' => 5, 'e-mail' => 'misty@pika.com', 'vorname' => 'misty', 'nachname' => 'kasumi', 
                    'passwort' => 'goldini', 'adresse' => 'sternstraße 4, azuria city', 'geb datum' => date('1997-01-01'), 
                    'groesse' => 1.60, 'schrittlaenge' => 80, 'geschlecht' => 'weiblich']);
	}
}

class RahmenSeeder extends Seeder {
	public function run()
	{
		DB::table('rahmen')->insert(['rahmen_typ' => 'StadtRad', 'preis' => 699]);
                DB::table('rahmen')->insert(['rahmen_typ' => 'TourenRad', 'preis' => 929]);
                DB::table('rahmen')->insert(['rahmen_typ' => 'BergRad', 'preis' => 799]);
                DB::table('rahmen')->insert(['rahmen_typ' => 'HollandRad', 'preis' => 699]);
	}
}

class KomponenteSeeder extends Seeder {
	public function run()
	{
            //Reifen
		DB::table('komponente')->insert(['komponente_id' => 1, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'SCHWALBE „Road Cruiser“']);
                DB::table('komponente')->insert(['komponente_id' => 2, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'SCHWALBE „Fat Frank“']);
                DB::table('komponente')->insert(['komponente_id' => 3, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'SCHWALBE „Kojak“']);
                DB::table('komponente')->insert(['komponente_id' => 4, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'SCHWALBE „Marathon plus“']);
                DB::table('komponente')->insert(['komponente_id' => 5, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'Offroad - Continental']);
                DB::table('komponente')->insert(['komponente_id' => 6, 'komponente_typ' => 'reifen',
                    'bezeichnung' => 'City - Schwalbe Big Apple']);
                
            //Sattel
                DB::table('komponente')->insert(['komponente_id' => 7, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'Selle Royal „Ariel Fitness“']);
                DB::table('komponente')->insert(['komponente_id' => 8, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'BROOKS „Flyer“']);
                DB::table('komponente')->insert(['komponente_id' => 9, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'BROOKS „B67“']);
                DB::table('komponente')->insert(['komponente_id' => 10, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'BRAVE „Classic Soft Touch“']);
                DB::table('komponente')->insert(['komponente_id' => 11, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'Sportlich']);
                DB::table('komponente')->insert(['komponente_id' => 12, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'Brooks Ledersattel']);
                DB::table('komponente')->insert(['komponente_id' => 13, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'Stylisch']);
                DB::table('komponente')->insert(['komponente_id' => 14, 'komponente_typ' => 'sattel',
                    'bezeichnung' => 'Kunstoffsattel']);
                
            //Lenker    
                DB::table('komponente')->insert(['komponente_id' => 15, 'komponente_typ' => 'lenker',
                    'bezeichnung' => 'Sportlicher Lenker']);
                DB::table('komponente')->insert(['komponente_id' => 16, 'komponente_typ' => 'lenker',
                    'bezeichnung' => 'Moderater Lenker']);
                DB::table('komponente')->insert(['komponente_id' => 17, 'komponente_typ' => 'lenker',
                    'bezeichnung' => 'Komfortabler Lenker']);
                
            //Griff    
                DB::table('komponente')->insert(['komponente_id' => 18, 'komponente_typ' => 'griff',
                    'bezeichnung' => 'Contec Ergo-Griff']);
                DB::table('komponente')->insert(['komponente_id' => 19, 'komponente_typ' => 'griff',
                    'bezeichnung' => 'BROOKS Ledergriffe']);
                DB::table('komponente')->insert(['komponente_id' => 20, 'komponente_typ' => 'griff',
                    'bezeichnung' => 'Selle Royal Mano']);
                DB::table('komponente')->insert(['komponente_id' => 21, 'komponente_typ' => 'griff',
                    'bezeichnung' => 'Brooks Ledergriffe']);
                DB::table('komponente')->insert(['komponente_id' => 22, 'komponente_typ' => 'griff',
                    'bezeichnung' => 'Kunststoffgriffe']);
                
                //Antrieb neu hinzugefügte Komponenten id(23 - 29)
                
            //Pedale    
                DB::table('komponente')->insert(['komponente_id' => 30, 'komponente_typ' => 'pedale',
                    'bezeichnung' => 'Pedale „VP-199“ von VP Components']);
                DB::table('komponente')->insert(['komponente_id' => 31, 'komponente_typ' => 'pedale',
                    'bezeichnung' => 'Pedale „VP-183“ von VP Components']);
                DB::table('komponente')->insert(['komponente_id' => 32, 'komponente_typ' => 'pedale',
                    'bezeichnung' => 'Pedale „LU-C17“ von WELLGO']);
                
                //Bremse neu hinzugefügte Komponenten id(33 - 35)
                
            //Gepäckträger    
                DB::table('komponente')->insert(['komponente_id' => 36, 'komponente_typ' => 'gepäckträger',
                    'bezeichnung' => 'Hebie „City II”, bis 40 kg']);
                DB::table('komponente')->insert(['komponente_id' => 37, 'komponente_typ' => 'gepäckträger',
                    'bezeichnung' => 'Tubus „Cargo Classic“, bis 40 kg']);
                
                
            //Korp    
                DB::table('komponente')->insert(['komponente_id' => 38, 'komponente_typ' => 'korp',
                    'bezeichnung' => 'BROOKS „Hoxton”']);
                DB::table('komponente')->insert(['komponente_id' => 39, 'komponente_typ' => 'gepäckträger',
                    'bezeichnung' => 'BASIL Weidenkorb']);
                
            //Licht
                DB::table('komponente')->insert(['komponente_id' => 40, 'komponente_typ' => 'licht',
                    'bezeichnung' => 'Lumotec IQ Cyo senso plus']);
                
            //Ständer    
                DB::table('komponente')->insert(['komponente_id' => 41, 'komponente_typ' => 'ständer',
                    'bezeichnung' => 'Seitenständer PLETSCHER „Zoom“']);
                DB::table('komponente')->insert(['komponente_id' => 42, 'komponente_typ' => 'ständer',
                    'bezeichnung' => 'Zweibeinständer Hebie „Rex S“']);
                
            //Kettenschutz    
                DB::table('komponente')->insert(['komponente_id' => 43, 'komponente_typ' => 'kettenschutz',
                    'bezeichnung' => 'Hebie "Chainbar"']);
                
            //Schloss    
                DB::table('komponente')->insert(['komponente_id' => 44, 'komponente_typ' => 'schloss',
                    'bezeichnung' => 'Trelock FS 300 Manufaktur']);
                
           //Antrieb erweiterung
                //Antrieb    
                DB::table('komponente')->insert(['komponente_id' => 23, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'sportlich: 1-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 24, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'sportlich: 2-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 25, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'moderat: 8-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 26, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'moderat: 8-Gang mit Rücktritt']);
                DB::table('komponente')->insert(['komponente_id' => 27, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'komfortabel: Stufenlos']);
                DB::table('komponente')->insert(['komponente_id' => 28, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'komfortabel: 11-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 29, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'komfortabel: 30-Gang Kettenschaltung']);
                DB::table('komponente')->insert(['komponente_id' => 45, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'Shimano Alivio 27-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 46, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'Shimano SLX 30-Gang']);
                DB::table('komponente')->insert(['komponente_id' => 47, 'komponente_typ' => 'antrieb',
                    'bezeichnung' => 'Shimano XT 30-Gang']);
                
            //Bremse erweiterung
                //bremse    
                DB::table('komponente')->insert(['komponente_id' => 33, 'komponente_typ' => 'bremse',
                    'bezeichnung' => 'SHIMANO Deore V-Brakes']);
                DB::table('komponente')->insert(['komponente_id' => 34, 'komponente_typ' => 'bremse',
                    'bezeichnung' => 'MAGURA HS11']);
                DB::table('komponente')->insert(['komponente_id' => 35, 'komponente_typ' => 'bremse',
                    'bezeichnung' => 'MAGURA HS33']);
                DB::table('komponente')->insert(['komponente_id' => 48, 'komponente_typ' => 'bremse',
                    'bezeichnung' => 'Scheibenbremse']);
	}
}

class FarbeSeeder extends Seeder {
	public function run()
	{
            DB::table('farbe')->insert(['farbe' => 'schwarz', 'hex' => '000000']);
            DB::table('farbe')->insert(['farbe' => 'weiß', 'hex' => 'ffffff']);
        }
}

class Rahmen_KomponenteSeeder extends Seeder {
	public function run()
	{
            //StadtRad Komponenten
            
                //Reifen
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 1, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 2, 'preis' => 19]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 3, 'preis' => 39]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 4, 'preis' => 39]);
            
                //Sattel
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 7, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 8, 'preis' => 59]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 9, 'preis' => 59]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 10, 'preis' => 19]);
            
                //Lenker
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 15, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 16, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 17, 'preis' => 0]);
            
                //Griff
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 18, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 19, 'preis' => 39]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 20, 'preis' => 29]);
            
                //Antrieb
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 23, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 24, 'preis' => 79]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 25, 'preis' => 139]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 26, 'preis' => 139]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 27, 'preis' => 229]);
            
                //Pedale
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 30, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 31, 'preis' => 9]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 32, 'preis' => 19]);
            
                //Bremse
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 33, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 34, 'preis' => 79]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 35, 'preis' => 99]);
            
                //Gepäckträger
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 36, 'preis' => 29]);
            
                //Korp
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 38, 'preis' => 99]);
            
                //Licht
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 40, 'preis' => 109]);
            
                //Ständer
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 41, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 42, 'preis' => 29]);
            
                //Kettenschutz
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 43, 'preis' => 23]);
            
                //Schloss
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'StadtRad', 'komponente_id' => 44, 'preis' => 79]);
            
            // ---------- Bis hier StadtRad
            
            
            //TourenRad Komponenten
            
                //Reifen
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 1, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 2, 'preis' => 19]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 3, 'preis' => 39]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 4, 'preis' => 39]);
            
                //Sattel
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 7, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 8, 'preis' => 59]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 9, 'preis' => 59]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 10, 'preis' => 19]);
            
                //Lenker
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 15, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 16, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 17, 'preis' => 0]);
            
                //Griff
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 18, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 19, 'preis' => 39]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 20, 'preis' => 29]);
            
                //Antrieb
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 25, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 27, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 28, 'preis' => 269]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 29, 'preis' => 219]);
            
                //Pedale
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 30, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 31, 'preis' => 9]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 32, 'preis' => 19]);
            
                //Bremse
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 33, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 34, 'preis' => 79]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 35, 'preis' => 99]);
            
                //Gepäckträger
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 36, 'preis' => 29]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 37, 'preis' => 79]);
            
                //Korp
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 38, 'preis' => 99]);
            
                //Licht
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 40, 'preis' => 119]);
            
                //Ständer
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 41, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 42, 'preis' => 29]);
            
                //Schloss
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'TourenRad', 'komponente_id' => 44, 'preis' => 79]);
            
            //------------ Bis hier TourenRad Komponente
            
            //BergRad Komponente
            
                //Reifen
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 5, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 6, 'preis' => 0]);
            
                //Sattel
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 11, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 12, 'preis' => 45]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 13, 'preis' => 25]);
            
                //Antrieb
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 45, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 46, 'preis' => 399]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 46, 'preis' => 599]);
            
                //Licht
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 40, 'preis' => 119]);
            
                //Bremsen
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 33, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 48, 'preis' => 100]);
            
                //Schloss
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'BergRad', 'komponente_id' => 44, 'preis' => 79]);
            
            //Bis hier BergRad Komponente --------------------
            
            //HollandRad Komponenten
                //Reifen
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 1, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 2, 'preis' => 0]);
            
                //Sattel
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 12, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 14, 'preis' => 0]);
            
                //Griff
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 21, 'preis' => 47]); 
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 22, 'preis' => 0]); 
            
                //Korp
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 38, 'preis' => 99]); 
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 39, 'preis' => 69]); 
            
                //Ständer
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 41, 'preis' => 0]);
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 42, 'preis' => 29]);
            
                //Schloss
            DB::table('rahmen_komponente')->insert(['rahmen_typ' => 'HollandRad', 'komponente_id' => 44, 'preis' => 79]);
        }
}


class EntwurfSeeder extends Seeder {
	public function run()
	{
            DB::table('entwurf')->insert(['entwurf_id' => 1, 'user_id' => 1, 'rahmen_typ' => 'StadtRad', 'rahmen_farbe' => 'schwarz' ]);
        }
}

class Entwurf_KomponenteSeeder extends Seeder {
	public function run()
	{
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 1, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 7, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 15, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 19, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 27, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 37, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 35, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 40, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 42, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 43, 'komponente_farbe' => 'schwarz' ]);
            DB::table('entwurf_komponente')->insert(['entwurf_id' => 1, 'komponente_id' => 44, 'komponente_farbe' => 'schwarz' ]);
            
            //Beispiel SQL-Befehl
            //select u.id, u.vorname, u.nachname, e.entwurf_id, e.rahmen_typ, k.komponente_typ, k.bezeichnung
            //from entwurf e, entwurf_komponente ek , user u, rahmen r, komponente k 
            //where u.id = e.user_id 
            //and e.entwurf_id = ek.entwurf_id
            //and e.rahmen_typ = r.rahmen_typ
            //and ek.komponente_id = k.komponente_id
        }
}
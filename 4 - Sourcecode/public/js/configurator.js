$(function() {
    var _sampleData = {
        'categories': [
            {
                'name': 'Rahmen',
                'alias': 'rahmen',
                'icon': 'fa-bicycle',
                'items': [
                    {
                        'name': 'Rahmen 1',
                        'alias': 'rahmen-1',
                        'price': 300.0
                    },
                    {
                        'name': 'Rahmen 2',
                        'alias': 'rahmen-2',
                        'price': 325.0
                    },
                    {
                        'name': 'Rahmen 3',
                        'alias': 'rahmen-3',
                        'price': 400.0
                    }
                ]
            },
            {
                'name': 'Lichter',
                'alias': 'lichter',
                'icon': 'fa-lightbulb-o',
                'items': [
                    {
                        'name': 'Licht 1',
                        'alias': 'licht-1',
                        'price': 10.0
                    },
                    {
                        'name': 'Licht 2',
                        'alias': 'licht-2',
                        'price': 15.0
                    },
                    {
                        'name': 'Licht 3',
                        'alias': 'licht-3',
                        'price': 5.0
                    }
                ]
            },
            {
                'name': 'Schlösser',
                'alias': 'schloesser',
                'icon': 'fa-lock',
                'items': [
                    {
                        'name': 'Schloss 1',
                        'alias': 'schloss-1',
                        'price': 10.0
                    },
                    {
                        'name': 'Schloss 2',
                        'alias': 'schloss-2',
                        'price': 40.0
                    },
                    {
                        'name': 'Schloss 3',
                        'alias': 'schloss-3',
                        'price': 75.99
                    }
                ]
            }
        ],
        'entwurf': []
    };

    var entwurf = [];
    var defaultView = 'overview';

    function loadConfiguratorData() {
        var data = _sampleData;

        var componentCategories = $(".component-categories");
        var listItemTemplate = componentCategories.find(".component-category.hidden").clone().hide().removeClass('hidden');
        var categoryViews = $(".component-category-view.hidden");
        var categoryViewTemplate = categoryViews.clone().removeClass('hidden');

        _sampleData.categories.forEach(function(category) {
            var listItem = listItemTemplate.clone();
            var categoryView = categoryViewTemplate.clone();

            listItem.find('a').prop('href', '#' + category.alias);
            listItem.find('.fa').addClass(category.icon);
            listItem.find('.category-name').html(category.name);

            categoryView.data('view', category.alias);
            categoryView.find('.category-name').html(category.name);

            var categoryListGroup = categoryView.find('.list-group');

            var itemTemplate = categoryView.find('.component-item.hidden').clone().removeClass('hidden');

            category.items.forEach(function(item) {
                var itemEntry = itemTemplate.clone();

                itemEntry.data('price', item.price);
                itemEntry.data('name', item.name);
                itemEntry.data('category', category.alias);
                itemEntry.find('.item-name').html(item.name);

                categoryListGroup.append(itemEntry);
            });

            componentCategories.before(categoryView);
            componentCategories.append(listItem);
            listItem.show();
        });

        listItemTemplate.remove();
        categoryViewTemplate.remove();
    }

    function calculateTotal() {
        var totalPrice = 0;

        entwurf.forEach(function(item) {
           totalPrice += item.price;
        });

        $(".total-price").html(totalPrice);
    }

    function addItem(item) {
        entwurf.push(item);

        console.log(item, entwurf);

        var componentList = $(".component-list");
        var listItem = componentList.find(".list-group-item.hidden").clone();

        listItem.data('item', item);
        listItem.find(".item-name").html(item.name);
        listItem.find(".item-price").html(item.price);
        listItem.find(".item-category").html(item.category);
        listItem.hide().removeClass('hidden');

        componentList.append(listItem);

        listItem.fadeIn(500);

        calculateTotal();
    }

    function removeItem(item) {
        entwurf.splice(entwurf.indexOf(item), 1);

        console.log(item, entwurf);

        calculateTotal();
    }

    $(document).on('click', 'a.component-category-link', function(e) {
        var el = $(this);
        var category = el.prop('href').split('#')[1];

        var categoryView = $(".component-category-view").filter(function(index) { return $(this).data('view') === category; });
        //var categoryView = $('.component-category-view[data-view="' + category + '"]');
        var visibleView = $('.component-category-view:visible');

        if(categoryView.length > 0) {
            visibleView.fadeOut(250, function() {
                categoryView.fadeIn(250);
            });
        } else {
            console.log("Unknown category view: " + category);
        }

        return false;
    });

    $(document).on('click', "a.component-item", function(e) {
        var el = $(this);
        var item = {
            price: parseFloat(el.data('price')),
            name: el.data('name'),
            category: el.data('category')
        };

        addItem(item);

        return false;
    });

    $(document).on('click', "a.item-action-remove", function(e) {
        var item = $(this).parents(".list-group-item");

        removeItem(item.data('item'));

        item.fadeOut(500, function() {
            $(this).remove();
        });

        return false;
    });

    loadConfiguratorData();

});

@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Entwürfe</div>

                    <div class="panel-body">
                        <h2>Entwürfe</h2>
                        <div class="row">
                            <div class="col-md-7">
                                <h3>1. Entwurf  
								<button class="btn btn-default" type="submit">entfernen</button>
								<button class="btn btn-default" type="submit">laden</button>
								</h3>
                                <img src="http://placehold.it/350x150" alt="Fahrrad" class="img-thumbnail" />
                            </div>

                        <div class="row">
                            <div class="col-md-13">
								<h3>Komponenten</h3>
                                <ul>
                                  <li><div>Stadtrad R310</div></li>
								  <li><div>EXUSTAR: Pedale Race E-PR200 schwarz</div></li>
								  <li><div>Schwalbe: MARATHON PLUS SmartGuard Drahtreifen Performance</div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
           
                        <div class="row">
                            <div class="col-md-7">
                                <h3>2. Entwurf  
								<button class="btn btn-default" type="submit">entfernen</button>
								<button class="btn btn-default" type="submit">laden</button>
								</h3>
                                <img src="http://placehold.it/350x150" alt="Fahrrad" class="img-thumbnail" />
                            </div>
                        <div class="row">
                            <div class="col-md-13">
							 <h3 >Komponenten</h3>
                                <ul>
								  <li><div>Rennrad I893</div></li>
								  <li><div>Lichtset hinten ROADSTER Black Combo ROADSTER </div></li>
								  <li><div>Schwalbe: RACING RALPH SnakeSkin TL-Easy faltbar</div></li>                              
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
 </div>
@endsection

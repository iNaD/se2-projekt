@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Ooops!</strong> Es bestehen einige Probleme mit deinen Eingaben.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
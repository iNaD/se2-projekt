<h3>Komponentenliste</h3>
<ul class="list-group component-list">
    <li class="list-group-item hidden clearfix" data-item="">
        <span class="item-name"></span>
        <span class="item-price"></span>&euro;
        (<span class="item-category"></span>)
        <span class="pull-right">
            <a href="#" class="btn btn-danger btn-xs item-action-remove"><i class="fa fa-trash"></i></a>
        </span>
    </li>
</ul>
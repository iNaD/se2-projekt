@extends('app')

@section('custom_js')
    @parent
    <script src="{{ asset('/js/configurator.js') }}"></script>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Konfigurator</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6"><h2 style="margin-top: 0;">Produktkonfigurator</h2></div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-default"><i class="fa fa-floppy-o"></i> Entwurf speichern</a>
                                    <a href="#" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Zum Warenkorb hinzufügen</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                @include('configurator._preview')
                                @include('configurator._component-listing')
                                <h3>Summe</h3>
                                <p><span class="total-price">0.0</span>&euro;</p>
                            </div>
                            <div class="col-md-5" style="height: 500px; overflow: auto;">
                                <div class="row">
                                    @include('configurator._component-picker')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-default"><i class="fa fa-floppy-o"></i> Entwurf speichern</a>
                                    <a href="#" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Zum Warenkorb hinzufügen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

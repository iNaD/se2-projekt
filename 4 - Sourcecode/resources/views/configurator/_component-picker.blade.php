<h3>Komponenten</h3>
<div class="col-md-12">

    <div class="row component-category-view hidden" style="display: none;" data-view="">
        <h4 class="category-name"></h4>

        <div class="list-group">
            <a href="#overview" class="list-group-item component-category-link active"><i class="fa fa-chevron-left"></i> <span class="pull-right">Zurück zur Kategorieübersicht</span></a>
            <a href="#" class="component-item list-group-item clearfix hidden" data-price="" data-name="" data-category=""><i class="fa fa-plus"></i> <span class="pull-right item-name"></span></a>
        </div>
    </div>

    <div class="row component-categories component-category-view" data-view="overview">
        <div class="col-md-6 component-category hidden">
            <a href="#" class="component-category-link">
                <div class="well well-sm text-center">
                    <i class="fa fa-3x"></i>
                    <span class="category-name"></span>
                </div>
            </a>
        </div>
    </div>
</div>
<?php

/**
 * Class ConfiguratorTest
 *
 * Enthält Tests für den Controller des Konfigurators
 *
 */

class ConfiguratorTest extends TestCase {

    /**
     * Testet, ob die Index Route für den Konfigurator funktioniert
     */
    public function testIndex()
    {
        $response = $this->action('GET', 'ConfiguratorController@index');

        $this->assertResponseOk();
    }

}

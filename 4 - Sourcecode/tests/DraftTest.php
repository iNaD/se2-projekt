<?php

/**
 * Class DraftTest
 *
 * Enthält Tests für den Controller der Entwürfe
 *
 */

class DraftTest extends TestCase {

    /**
     * Testet, ob die Index Route für die Entwürfe funktioniert
     */
    public function testIndex()
    {
        $response = $this->action('GET', 'DraftController@index');

        $this->assertResponseOk();
    }

}
